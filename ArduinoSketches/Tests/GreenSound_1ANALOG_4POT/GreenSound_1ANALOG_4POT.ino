/* 
 *  This software takes 1 analog in that comes from a plant
 *  It translates the input signal in MIDI and has 3 analog controls
 *  (eg. Potentiometers) to change speed pitch and velocity of the notes
 */
#include <MIDI.h>
MIDI_CREATE_DEFAULT_INSTANCE();

// Variables:
byte note = 0,note2=0;  // The MIDI note value to be played
const int PIN_plant1 = A5;  // Analog input pin that the potentiometer is attached to
const int PIN_plant2 = A6;  // Analog input pin that the potentiometer is attached to
const int PIN_sensitivity = A0;  // Analog input pin that the potentiometer is attached to
const int PIN_pitch = A1;  // Analog input pin that the potentiometer is attached to
const int PIN_velocity = A2;  // Analog input pin that the potentiometer is attached to
const int PIN_samplingSpeed = A3;  // Analog input pin that the potentiometer is attached to
const int PIN_onOffInterval = A4; 
const int PIN_LED_ONOFF = 2; 

const int midiOutRX = 2;
const int midiOutTX = 3;
int VAL_plant1=0,VAL_plant2=0,VAL_sensitivity=0,VAL_pitch=0,VAL_velocity=0,VAL_samplingSpeed=10,VAL_onOffInterval=10;
int max_in_frame=0;
int max_in_frame2=0;
int min_in_frame=0;
int min_in_frame2=0;
int mean2=0;
int analogValue = 0;         // value read from the pot
int mean_sens_margins=10;
int dimmerMult=1;  // Multiplier of dimmer sensitivity for delay parameters
int i=0,mean_samples=10;
int computed=0,computed2=0,audio=0,mean=0;
//software serial instantiate midiSerial object
//SoftwareSerial midiSerial(midiOutRX, midiOutTX); // digital pins that we'll use for soft serial RX & TX
int noteBuffer[10][3], bufIdx=0 , bufIdy=0;
  void setup() {
    //  Set MIDI baud rate:
    Serial.begin(115200); // use if using with ATmega328 (uno, mega, nano...)
    VAL_onOffInterval = 1;
    pinMode(PIN_LED_ONOFF, OUTPUT);
    digitalWrite(PIN_LED_ONOFF, HIGH);
  }

void loop() {
  mean=0;
  mean2=0;
  max_in_frame=0;
  min_in_frame=0;
  // put your main code here, to run repeatedly:
  for (i = 0; i < mean_samples; i ++){
      // Read ANALOG VALUE from PLANT
      VAL_plant1 = analogRead(PIN_plant1);
      VAL_plant2= analogRead(PIN_plant2);
      mean += VAL_plant1;
      mean2 += VAL_plant2;
      // Store the max and the min (if reached)
      if(max_in_frame<VAL_plant1){
        max_in_frame=VAL_plant1;
      }
      if(min_in_frame>VAL_plant1){
        min_in_frame=VAL_plant1;
      } 
      if(max_in_frame2<VAL_plant2){
        max_in_frame2=VAL_plant2;
      }
      if(min_in_frame2>VAL_plant2){
        min_in_frame2=VAL_plant2;
      }    
  }
  
  computed = mean/mean_samples;
  computed2 = mean2/mean_samples;
  VAL_sensitivity = analogRead(PIN_sensitivity);
  VAL_pitch = analogRead(PIN_pitch);
  VAL_velocity = analogRead(PIN_velocity);
  note = map(computed, 1, 1024, 30, 90);
  note2 = map(computed2, 1, 1024, 30, 90);
  VAL_sensitivity = map (VAL_sensitivity, 1,1024,1,100);
  VAL_velocity = map (VAL_velocity,1,1024,1,127);
  /*MIDI.sendNoteOn(note, VAL_velocity, 1); // note, velocity, channel
  delay(1200-VAL_onOffInterval*10);
  MIDI.sendNoteOff(note, VAL_velocity, 1); // note, velocity, channel
  delay(1200-VAL_samplingSpeed*10);*/
  if ( abs( VAL_plant1 - computed ) > VAL_sensitivity/2 ){
    //Serial.println(computed+100);
    if ( VAL_samplingSpeed > 0 ){
      VAL_samplingSpeed--;
      if ( VAL_samplingSpeed <= 0 ){
        VAL_samplingSpeed = analogRead( PIN_samplingSpeed );
        if(VAL_samplingSpeed<2)VAL_samplingSpeed=1;
        VAL_onOffInterval = analogRead( PIN_onOffInterval );
        MIDI.sendNoteOn( note + sqrt( VAL_pitch ), VAL_velocity, 1 ); //note, velocity, channel
        MIDI.sendNoteOn( note2 + sqrt( VAL_pitch ), VAL_velocity, 2 ); //note, velocity, channel
        delay( VAL_onOffInterval );
        MIDI.sendNoteOff( note + sqrt(VAL_pitch) , VAL_velocity , 1); //note, velocity, channel
        MIDI.sendNoteOff( note2 + sqrt(VAL_pitch) , VAL_velocity , 2); //note, velocity, channel
      }
    }
  }
}
