/**
 * This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * This sketch takes from interface analogical and pass the tension to the 
 * sound system via MIDI
 * Is part of the GreenSound project
 * Creator: Alain Bindele <alain.bindele@gmail.com>
 * Site: greensound.pc-go.it
 * Instangram: greensoundproject
 */

#include <MIDI.h>
MIDI_CREATE_DEFAULT_INSTANCE();


// Variables:
byte note = 0;  // The MIDI note value to be played
const int anaCh1 = A0;  // Analog input pin that the potentiometer is attached to
//const int anaCh2 = A2;  // Analog input pin that the potentiometer is attached to
const int anaCh1Pitch = A1;  // Analog input pin that the potentiometer is attached to
const int anaCh1Velocity = A2;  // Analog input pin that the potentiometer is attached to
const int anaCh1Speed = A3;  // Analog input pin that the potentiometer is attached to

const int anaCh2Pitch = A5;  // Analog input pin that the potentiometer is attached to
const int anaCh2Velocity = A6;  // Analog input pin that the potentiometer is attached to
const int midiOutRX = 2;
const int midiOutTX = 3;
int samples_interval=2048;
int max_in_frame=0;
int min_in_frame=0;
int analogValue = 0;         // value read from the pot
int ch1p=0,ch1v=0,ch1s=0;
int i=0,samples=10;
int computed=0,audio=0,mean=0;

//software serial instantiate midiSerial object
//SoftwareSerial midiSerial(midiOutRX, midiOutTX); // digital pins that we'll use for soft serial RX & TX

  void setup() {
    //  Set MIDI baud rate:
    Serial.begin(115200); // use if using with ATmega328 (uno, mega, nano...)
    //midiSerial.begin(31250);
  }


void loop() {
  mean=0;
  // put your main code here, to run repeatedly:
  for (i = 0; i < samples; i ++){
      analogValue = analogRead(anaCh1);
      mean += analogValue;
  }
  ch1p = analogRead(1);
  ch1v = analogRead(2);
  ch1s = analogRead(3);
  computed = mean/samples;
  
  note = map(computed, 1, 1024, 30, 90);
  //ch1v = map(ch1v, 1, 1024, 50, 127);
  ch1v*=10;
  //ch1s = map(pow(ch1s,1.5), 1, 32768, 200, 2000);*/
  
  MIDI.sendNoteOn(note, ch1v, 1); // note, velocity, channel
  MIDI.sendNoteOn(note+ch1v, ch1v, 2); // note, velocity, channel
  delay(50);
  MIDI.sendNoteOff(note, ch1v, 1); // note, velocity, channel
  MIDI.sendNoteOff(note+ch1v, ch1v, 2); // note, velocity, channel
  delay(50);

  Serial.print("A0:");
  Serial.print(analogValue);
  Serial.print(",A1:");
  Serial.print(ch1p);
  Serial.print(",A2:");
  Serial.print(ch1v);
  Serial.print(",A3:");
  Serial.print(ch1s);
  Serial.println(""); 
}
