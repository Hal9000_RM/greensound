/* 
 *  This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
 *  This software takes 1 analog in that comes from a plant
 *  It translates the input signal in MIDI and has 3 analog controls
 *  (eg. Potentiometers) to change speed pitch and velocity of the notes
 *  
 * Creator: Alain Bindele <alain.bindele@gmail.com>
 * Site: greensound.pc-go.it
 * Instangram: greensoundproject
 */
#include <MIDI.h>
#include <SoftwareSerial.h>
MIDI_CREATE_DEFAULT_INSTANCE();

// Variables:
byte note = 0,note2=0;  // The MIDI note value to be played
const int PIN_plant1 = A5;  // Analog input pin that the potentiometer is attached to
const int PIN_sensitivity = A0;  // Analog input pin that the potentiometer is attached to
const int PIN_pitch = A1;  // Analog input pin that the potentiometer is attached to
const int PIN_velocity = A2;  // Analog input pin that the potentiometer is attached to
const int PIN_samplingSpeed = A3;  // Analog input pin that the potentiometer is attached to
const int PIN_onOffInterval = A4; 
const int PIN_LED_ONOFF = 2; 

const int midiOutRX = 2;
const int midiOutTX = 3;
int VAL_plant1=0,VAL_plant2=0,VAL_sensitivity=0,VAL_pitch=0,VAL_velocity=0,VAL_samplingSpeed=10,VAL_onOffInterval=10;

int mean2=0;
int analogValue = 0;         // value read from the pot
int mean_sens_margins=10;
int dimmerMult=1;  // Multiplier of dimmer sensitivity for delay parameters
int i=0,mean_samples=10;
int computed=0,computed2=0,audio=0,mean=0;
//software serial instantiate midiSerial object
SoftwareSerial midiSerial(midiOutRX, midiOutTX); // digital pins that we'll use for soft serial RX & TX
int noteBuffer[10][3], bufIdx=0 , bufIdy=0;
  void setup() {
    //  Set MIDI baud rate:
    Serial.begin(115200); // use if using with ATmega328 (uno, mega, nano...)
    midiSerial.begin(31250); // Initialize the midi port for midijack
    VAL_onOffInterval = 1;
    pinMode(PIN_LED_ONOFF, OUTPUT);
    digitalWrite(PIN_LED_ONOFF, HIGH);
  }

void loop() {
  mean=0;
  
  // put your main code here, to run repeatedly:
  for (i = 0; i < mean_samples; i ++){
      // Read ANALOG VALUE from PLANT
      VAL_plant1 = analogRead(PIN_plant1);
      mean += VAL_plant1;
      // Store the max and the min (if reached)
  }
  
  computed = mean/mean_samples;
  VAL_sensitivity = analogRead(PIN_sensitivity);
  VAL_pitch = analogRead(PIN_pitch);
  VAL_velocity = analogRead(PIN_velocity);
  note = map(computed, 1, 1024, 30, 90);
  VAL_sensitivity = map (VAL_sensitivity, 1,1024,1,100);
  VAL_velocity = map (VAL_velocity,1,1024,1,127);

  if ( abs( VAL_plant1 - computed ) > VAL_sensitivity/2 ){
    if ( VAL_samplingSpeed > 0 ){
      VAL_samplingSpeed--;
      if ( VAL_samplingSpeed <= 0 ){
        VAL_samplingSpeed = analogRead( PIN_samplingSpeed );
        if(VAL_samplingSpeed<2)VAL_samplingSpeed=1;
        VAL_onOffInterval = analogRead( PIN_onOffInterval );
        MIDI.sendNoteOn( note + sqrt( VAL_pitch ), VAL_velocity, 1 ); //note, velocity, channel
        noteOn(0x90, note, 0x45);
        delay( VAL_onOffInterval );
        MIDI.sendNoteOff( note + sqrt(VAL_pitch) , VAL_velocity , 1); //note, velocity, channel
        noteOn(0x90, note, 0x00);
      }
    }
  }
}

  //  plays a MIDI note.  Doesn't check to see that
  //  cmd is greater than 127, or that data values are  less than 127:
  void noteOn(byte cmd, byte data1, byte data2) {
    midiSerial.write(cmd);
    midiSerial.write(data1);
    midiSerial.write(data2);
    /*
    Serial.print("cmd:");
    Serial.print(cmd);
    Serial.print(",data1:");
    Serial.print(data1);
    Serial.print(",data2:");
    Serial.print(data2);
    Serial.println("");
    */  
  }
