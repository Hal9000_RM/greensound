/***
 * This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    This program read the analog input and makes the mean
    it works tweaking the different parameters and can be potentially 
    a code-taking piece of cake ^_^

 * Creator: Alain Bindele <alain.bindele@gmail.com>
 * Site: greensound.pc-go.it
 * Instangram: greensoundproject
 */

int analogValue1 = 0;         // value read from the Analog 1
int analogValue2 = 0;         // value read from the Analog 2
int analogValue3 = 0;         // value read from the Analog 3
int analogValue4 = 0;         // value read from the Analog 3
int i = 0,samples = 25;
int computed=0,audio=0,mean=0,oldMean=0;
int margin = 50;

//software serial instantiate midiSerial object
//SoftwareSerial midiSerial(midiOutRX, midiOutTX); // digital pins that we'll use for soft serial RX & TX

void setup() {
  //  Set MIDI baud rate:
  Serial.begin(115200); // use if using with ATmega328 (uno, mega, nano...)
  //midiSerial.begin(31250);
}

  
void loop() {
  mean=0;
  // put your main code here, to run repeatedly:
  for (i = 0; i < samples; i ++){
      analogValue1 = analogRead(A0);
      mean += analogValue1;
      if ( abs(analogValue-oldMean)>margin){
        // Do something if margin is passed
      }
  }
  analogValue2 = analogRead(A1);
  analogValue3 = analogRead(A2);
  analogValue4 = analogRead(A3);
  computed = mean/samples;
  oldMean=computed;
  delay(25);
  //Serial.print("A0:");
  Serial.println(analogValue1);
  /*Serial.print(",A1:");
  Serial.print(analogValue2);
  Serial.print(",A2:");
  Serial.print(analogValue3);
  Serial.print(",A3:");
  Serial.println(analogValue4);*/
}
